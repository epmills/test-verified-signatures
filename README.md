# Test Verified Signatures

- Test the ability to commit with gnupg verified signatures.
- A second test after updated email address
- A third test after redoing keyring
- A fourth test after revamping mba
- A fifth test after creating signing-only key
- A sixth test after adding new key to account
- A seventh test after new email confirmed
- An eighth test after changing commit email
- A ninth test after reducing file permissions
- A tenth test after adding hardware key
- An eleventh test after adding hardware key
- A twelfth test after adding hardware key
- A thirteenth test after adding hardware key

